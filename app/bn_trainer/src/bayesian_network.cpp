/*
 * bayesian_network.cpp
 *
 *  Created on: Mar 14, 2016
 *      Author: veronica
 */


#include "bayesian_network.hpp"


void BNetwork::init_params(
		int nFeatures_max,
		double distribution_std_max)
{
	nFeatures_max_ = nFeatures_max;
	distribution_std_max_ = distribution_std_max;
}

/*
The function compute the posterior probability of a failure in KLT Tracker,
returns 0 for not failure and 1 for failure
 */

void BNetwork::init_network(
		double nFeatSigma,				// nFeatures_max / 3
		double nFeatSigmaF,				// 4
		double percLostFeatSigma,		// 15
		double percLostFeatSigmaF,		// 30
		double validityHomographySigma,	// 0.01
		double validityHomographySigmaF,	// 0.6
		double stdDistributionSigma,		// 15
		double stdDistributionSigmaF,	// 150
		double bhDistanceSigma,			// 0.1
		double bhDistanceSigmaF)			// 0.6
{
	// configure the network
	bn.set_number_of_nodes(6);
	bn.add_edge(F, A);
	bn.add_edge(F, B);
	bn.add_edge(F, C);
	bn.add_edge(F, D);
	bn.add_edge(F, E);

	// set the number of state of each nodes
	dlib::bayes_node_utils::set_node_num_values(bn, A, nFeatures_max_);
	dlib::bayes_node_utils::set_node_num_values(bn, B, 100); // percentage
	dlib::bayes_node_utils::set_node_num_values(bn, C, 2);
	dlib::bayes_node_utils::set_node_num_values(bn, D, distribution_std_max_);
	dlib::bayes_node_utils::set_node_num_values(bn, E, 100); // Percentage
	dlib::bayes_node_utils::set_node_num_values(bn, F, 2);

	dlib::assignment parent_state;
	// Now we will enter all the conditional probability information for each node.
	// Each node's conditional probability is dependent on the state of its parents.
	// To specify this state we need to use the assignment object.  This assignment
	// object allows us to specify the state of each nodes parents.

	/*
	 * IF F = 0
	 */

	double p_F;
	double p_F_sum = 0.0;

	parent_state.clear();

	dlib::bayes_node_utils::set_node_probability(bn, F, 0, parent_state, 0.5);
	dlib::bayes_node_utils::set_node_probability(bn, F, 1, parent_state, 0.5);

	parent_state.add(F, 0);

	/*
	 * A
	 */

	// compute all value
	for(int a = 0; a < nFeatures_max_; a++)
	{
		p_F = gsl_ran_gaussian_pdf (a, nFeatSigma);
		p_F_sum += p_F;

	}
	// assign pdf values
	//	std::cout << "a : ";
	for(int a = 0; a < nFeatures_max_; a++)
	{
		p_F = gsl_ran_gaussian_pdf (a, nFeatSigma);
		dlib::bayes_node_utils::set_node_probability(
				bn, A, nFeatures_max_ - a -1, parent_state, p_F / p_F_sum);
		//		std::cout << nFeatures_max_ - a -1 << ", ";


	}
	//	std::cout << std::endl;

	/*
	 * B
	 */

	// compute all value
	p_F_sum = 0.0;
	for(int b = 0; b < 100; b++)
	{
		//		double b_temp = b / 100;
		p_F = gsl_ran_gaussian_pdf (b, percLostFeatSigma);
		p_F_sum += p_F;
	}

	// assign pdf values
	//	std::cout << "b : ";
	for(int b = 0; b < 100; b++)
	{
		//		double b_temp = b / 100;
		p_F = gsl_ran_gaussian_pdf (b, percLostFeatSigma);
		dlib::bayes_node_utils::set_node_probability(
				bn, B, b, parent_state, p_F / p_F_sum);

		//		std::cout << b << ", ";
	}
	//	std::cout << std::endl;


	/*
	 * C
	 */
	// homography = 0 -- invalid
	// homography = 1 -- valid

	dlib::bayes_node_utils::set_node_probability(
			bn, C, 0, parent_state, validityHomographySigma);
	dlib::bayes_node_utils::set_node_probability(
			bn, C, 1, parent_state, 1.0 - validityHomographySigma);

	/*
	 * D 15
	 */
	// compute all value
	p_F_sum = 0.0;
	for(int d = 0; d < distribution_std_max_; d++)
	{
		//		double d_temp = d / distribution_std_max_;
		p_F = gsl_ran_gaussian_pdf (d, stdDistributionSigma);
		p_F_sum += p_F;
	}
	// assign pdf values
	//	std::cout << "d : ";
	for(int d = 0; d < distribution_std_max_; d++)
	{
		//		double d_temp = d / distribution_std_max_;
		p_F = gsl_ran_gaussian_pdf (d, stdDistributionSigma);
		dlib::bayes_node_utils::set_node_probability(
				bn, D, d, parent_state, p_F/p_F_sum);

		//		std::cout << d << ", ";
	}
	//	std::cout << std::endl;


	/*
	 * E
	 */
	// compute all value
	p_F_sum = 0.0;
	for(int b = 0; b < 100; b++)
	{
		p_F = gsl_ran_gaussian_pdf (b, bhDistanceSigma);
		p_F_sum += p_F;
	}

	// assign pdf values
	for(int b = 0; b < 100; b++)
	{
		p_F = gsl_ran_gaussian_pdf (b, bhDistanceSigma);
		dlib::bayes_node_utils::set_node_probability(
				bn, E, b, parent_state, p_F / p_F_sum);
	}


	/*
	 * IF F = 1
	 */

	// tracking failure
	parent_state[F] = 1;
	p_F = 0.0;
	p_F_sum = 0.0;

	/*
	 * A 4
	 */
	// compute all value
	for(int a = 0; a < nFeatures_max_; a++)
	{
		p_F = gsl_ran_exponential_pdf (a, nFeatSigmaF);
		p_F_sum += p_F;

	}
	// assign pdf values
	//	std::cout << "a : ";
	for(int a = 0; a < nFeatures_max_; a++)
	{
		p_F = gsl_ran_exponential_pdf (a, nFeatSigmaF);
		dlib::bayes_node_utils::set_node_probability(
				bn, A, a , parent_state, p_F / p_F_sum);

		//		std::cout << a << ", ";
	}
	//	std::cout << std::endl;

	/*
	 * B 18
	 */
	// compute all value
	p_F_sum = 0.0;
	for(int b = 0; b < 100; b++)
	{
		//		double b_temp = b / 100;
		p_F = gsl_ran_gaussian_pdf (b,percLostFeatSigmaF);
		p_F_sum += p_F;
	}

	// assign pdf values
	//	std::cout << "b : ";
	for(int b = 0; b < 100; b++)
	{
		//		double b_temp = b / 100;
		p_F = gsl_ran_gaussian_pdf (b, percLostFeatSigmaF);
		dlib::bayes_node_utils::set_node_probability(
				bn, B, 99- b, parent_state, p_F / p_F_sum);

		//		std::cout << 99 - b << ", ";
	}
	//	std::cout << std::endl;

	/*
	 * C
	 */
	// homography = 0 -- invalid
	// homography = 1 -- valid

	dlib::bayes_node_utils::set_node_probability(
			bn, C, 0, parent_state, validityHomographySigmaF);
	dlib::bayes_node_utils::set_node_probability(
			bn, C, 1, parent_state, 1.0 - validityHomographySigmaF);

	/*
	 * D 90
	 */
	// compute all value
	p_F_sum = 0.0;
	for(int d = 0; d < distribution_std_max_; d++)
	{
		//		double d_temp = d / distribution_std_max_;
		p_F = gsl_ran_gaussian_pdf (d, stdDistributionSigmaF);
		p_F_sum += p_F;

	}

	// assign pdf values
	//	std::cout << "d : ";
	for(int d = 0; d < distribution_std_max_; d++)
	{
		//		double d_temp = d / distribution_std_max_;
		p_F = gsl_ran_gaussian_pdf (d, stdDistributionSigmaF);
		dlib::bayes_node_utils::set_node_probability(
				bn, D, distribution_std_max_ - d -1, parent_state, p_F/p_F_sum);
//		std::cout << p_F/p_F_sum << std::endl;
		//		std::cout << distribution_std_max_ - d -1 << ", ";

	}
	//	std::cout << std::endl;

	/*
	 * E
	 */
	// compute all value
	p_F_sum = 0.0;
	for(int b = 0; b < 100; b++)
	{
		p_F = gsl_ran_gaussian_pdf (b,bhDistanceSigmaF);
		p_F_sum += p_F;
	}

	// assign pdf values
	for(int b = 0; b < 100; b++)
	{
		p_F = gsl_ran_gaussian_pdf (b, bhDistanceSigmaF);
		dlib::bayes_node_utils::set_node_probability(
				bn, E, 99- b, parent_state, p_F / p_F_sum);
	}


	create_moral_graph(bn, join_tree);
	create_join_tree(join_tree, join_tree);
}

double BNetwork::compute_joint_probability(
		int nFeat,
		double percLostFeat,
		bool validityHomography,
		double stdDistribution,
		double bhDistance)
{


	// nfeat
	if(nFeat < 0)
		nFeat = 0;
	if(nFeat > nFeatures_max_-1)
		nFeat = nFeatures_max_ -1;


	int temp_perc = (int)(percLostFeat*100.0);
	int temp_std = (int)stdDistribution;
	int temp_bhDistance = (int)(bhDistance*100.0);

	// percLostFeat
	if(temp_perc < 0)
		temp_perc = 0;
	if(temp_perc > 99)
		temp_perc = 99;
	// stdDistribution
	if(temp_std < 0)
		temp_std = 0;
	if(temp_std > distribution_std_max_ -1)
		temp_std = distribution_std_max_ -1;


	dlib::bayes_node_utils::set_node_value(bn, A, nFeat);
	dlib::bayes_node_utils::set_node_as_evidence(bn, A);
	dlib::bayes_node_utils::set_node_value(bn, B, temp_perc);
	dlib::bayes_node_utils::set_node_as_evidence(bn, B);
	dlib::bayes_node_utils::set_node_value(bn, C, validityHomography);
	dlib::bayes_node_utils::set_node_as_evidence(bn, C);
	dlib::bayes_node_utils::set_node_value(bn, D, temp_std);
	dlib::bayes_node_utils::set_node_as_evidence(bn, D);
	dlib::bayes_node_utils::set_node_value(bn, E, temp_bhDistance);
	dlib::bayes_node_utils::set_node_as_evidence(bn, E);

	dlib::bayesian_network_join_tree solution_with_evidence(bn, join_tree);

	double prob = solution_with_evidence.probability(F)(1);

#ifdef DEBUG
	std::cout << "-----------"  << std::endl;
	std::cout << "N features              : " << nFeat << std::endl;
	std::cout << "Percentage lost features: " << percLostFeat << std::endl;
	std::cout << "Homography valid        : " << validityHomography << std::endl;
	std::cout << "STD distribution        : " << stdDistribution << std::endl;
	std::cout << "                           " << stdDistribution << std::endl;
	std::cout << "Failure PROBABILITY: " << prob << std::endl;
	std::cout << "-----------"  << std::endl;
#endif

	return prob;
}


//int main(int argc, char **argv)
//{
//	BNetwork BNet;
//
//	BNet.init_params(100, 360.0);
//	BNet.init_network();
//
//	BNet.compute_joint_probability(3, 0.7, false, 10);
//	BNet.compute_joint_probability(30, 0.2, true, 10);
//	BNet.compute_joint_probability(50, 0.5, true, 100);
//	BNet.compute_joint_probability(10, 0.2, true, 200);
//}


