/*
 * INCLUDES
 */

#include <iostream>
#include <fstream>
#include <list>
#include <stdlib.h>

#include "bayesian_network.hpp"


/*
 * DEFINES
 */

#define DEBUG

#define HEADER_N_FEATURES		"n_features"
#define HEADER_PERC_FEATURES	"perc_features"
#define HEADER_HOMOGRAPHY_VALID	"homography_valid"
#define HEADER_STD_OF			"std_of"
#define HEADER_BH_DISTANCE		"bh_distance"
#define HEADER_FAIL				"fail"
#define HEADER_EST_FAIL			"estimate_fail"

#define FAILURE_THRESHOLD		0.4

//#define RNG_SEED				0

#define SCORE_FAIL		4.0
#define SCORE_NON_FAIL	1.0


/*
 * TYPES
 */

typedef struct
{
	int n_features;
	double perc_features;
	bool homography_valid;
	double std_of;
	double bh_distance;
	bool fail;
	bool estimated_fail;
}DataPoint;

typedef struct
{
	int nFeatures_max;
	double distribution_std_max;
	double nFeatSigma;
	double nFeatSigmaF;
	double percLostFeatSigma;
	double percLostFeatSigmaF;
	double valodityHomographySigma;
	double valodityHomographySigmaF;
	double stdDistributionSigma;
	double stdDistributionSigmaF;
	double bhDistanceSigma;
	double bhDistanceSigmaF;
}BNetParams;


/*
 * FUNCTIONS
 */

void usage()
{
	std::cout << "NAME\n";
	std::cout << "      Bayesian network trainer\n";
	std::cout << "\n";
	std::cout << "SYNOPSIS\n";
	std::cout << "      usage: bn_trainer [INPUT_DATA]\n";
	std::cout << "\n";
	std::cout << "DESCRIPTION\n";
	std::cout << "\n";
	std::cout << "      INPUT_DATA\n";
	std::cout << "            CSV file containing the input data\n";
	std::cout << std::endl;
}

bool loadData(std::list<DataPoint> &data, const std::string &filename)
{
	std::ifstream file;
	char line[256];
	DataPoint dataPoint;

	// Open file
	file.open(filename.c_str());
	if (!file)
	{
		std::cout << "Error opening file" << std::endl;
		return false;
	}

	// Read header
	//file.getline(line, 256);
	// TODO: Check header

	// Read line by line
	while (!file.eof())
	{
		file.getline(line, 256, ',');
		if (file.eof()) break;
		dataPoint.n_features = atoi(line);

		file.getline(line, 256, ',');
		if (file.eof()) break;
		dataPoint.perc_features = atof(line);

		file.getline(line, 256, ',');
		if (file.eof()) break;
		dataPoint.homography_valid = atoi(line);

		file.getline(line, 256, ',');
		if (file.eof()) break;
		dataPoint.std_of = atof(line);

		file.getline(line, 256, ',');
		if (file.eof()) break;
		dataPoint.bh_distance = atof(line);

		file.getline(line, 256);
		if (file.eof()) break;
		dataPoint.fail = atoi(line);

		data.push_back(dataPoint);
	}

	// Close file
	file.close();

	// Return OK
	return true;
}

void printData(std::list<DataPoint> &data)
{
	// Print header
	std::cout << HEADER_N_FEATURES << ",";
	std::cout << HEADER_PERC_FEATURES << ",";
	std::cout << HEADER_HOMOGRAPHY_VALID << ",";
	std::cout << HEADER_STD_OF << ",";
	std::cout << HEADER_BH_DISTANCE << ",";
	std::cout << HEADER_FAIL << ",";
	std::cout << HEADER_EST_FAIL << std::endl;

	// Print data
	std::list<DataPoint>::iterator it;

	for (it = data.begin(); it != data.end(); it++)
	{
		std::cout << it->n_features << ",";
		std::cout << it->perc_features << ",";
		std::cout << it->homography_valid << ",";
		std::cout << it->std_of << ",";
		std::cout << it->bh_distance << ",";
		std::cout << it->fail << ",";
		std::cout << it->estimated_fail << std::endl;
	}
}

void setRandomParams(
	const gsl_rng *rng,
	BNetParams &params,
	BNetParams &paramsMin,
	BNetParams &paramsMax)
{
	params.nFeatSigma =
		paramsMin.nFeatSigma +
		(paramsMax.nFeatSigma - paramsMin.nFeatSigma) *
		gsl_rng_uniform(rng);
	params.nFeatSigmaF =
		paramsMin.nFeatSigmaF +
		(paramsMax.nFeatSigmaF - paramsMin.nFeatSigmaF) *
		gsl_rng_uniform(rng);

	params.percLostFeatSigma =
		paramsMin.percLostFeatSigma +
		(paramsMax.percLostFeatSigma - paramsMin.percLostFeatSigma) *
		gsl_rng_uniform(rng);
	params.percLostFeatSigmaF =
		paramsMin.percLostFeatSigmaF +
		(paramsMax.percLostFeatSigmaF - paramsMin.percLostFeatSigmaF) *
		gsl_rng_uniform(rng);

	params.valodityHomographySigma =
		paramsMin.valodityHomographySigma +
		(paramsMax.valodityHomographySigma - paramsMin.valodityHomographySigma) *
		gsl_rng_uniform(rng);
	params.valodityHomographySigmaF =
		paramsMin.valodityHomographySigmaF +
		(paramsMax.valodityHomographySigmaF - paramsMin.valodityHomographySigmaF) *
		gsl_rng_uniform(rng);

	params.stdDistributionSigma =
		paramsMin.stdDistributionSigma +
		(paramsMax.stdDistributionSigma - paramsMin.stdDistributionSigma) *
		gsl_rng_uniform(rng);
	params.stdDistributionSigmaF =
		paramsMin.stdDistributionSigmaF +
		(paramsMax.stdDistributionSigmaF - paramsMin.stdDistributionSigmaF) *
		gsl_rng_uniform(rng);

	params.bhDistanceSigma =
		paramsMin.bhDistanceSigma +
		(paramsMax.bhDistanceSigma - paramsMin.bhDistanceSigma) *
		gsl_rng_uniform(rng);
	params.bhDistanceSigmaF =
		paramsMin.bhDistanceSigmaF +
		(paramsMax.bhDistanceSigmaF - paramsMin.bhDistanceSigmaF) *
		gsl_rng_uniform(rng);
}

double evaluateBNet(
	std::list<DataPoint> &data, BNetwork &bNet, BNetParams &params)
{
	if (data.empty())
		return 0.0;

	// Init Bayesian Network
	bNet.init_network(
		params.nFeatSigma, params.nFeatSigmaF,
		params.percLostFeatSigma, params.percLostFeatSigmaF,
		params.valodityHomographySigma, params.valodityHomographySigmaF,
		params.stdDistributionSigma, params.stdDistributionSigmaF,
		params.bhDistanceSigma, params.bhDistanceSigmaF);

	// Iterate in the points
	double failure_jont_probability;
	std::list<DataPoint>::iterator it;
	double score = 0.0;
	double counter = 0.0;

	for (it = data.begin(); it != data.end(); it++)
	{
		failure_jont_probability = bNet.compute_joint_probability(
			it->n_features,
			it->perc_features,
			it->homography_valid,
			it->std_of,
			it->bh_distance);
		it->estimated_fail = failure_jont_probability > FAILURE_THRESHOLD;

		if (it->fail)
			counter += SCORE_FAIL;
		else
			counter += SCORE_NON_FAIL;

		if (it->estimated_fail && it->fail)
			score += SCORE_FAIL;
		else if (!it->estimated_fail && !it->fail)
			score += SCORE_NON_FAIL;
	}

	return score / counter;
}

void printParams(BNetParams &params)
{
	std::cout << "  - N features: " << params.nFeatSigma;
	std::cout << "/" << params.nFeatSigmaF << "\n";
	std::cout << "  - Perc features: " << params.percLostFeatSigma;
	std::cout << "/" << params.percLostFeatSigmaF << "\n";
	std::cout << "  - Homography: " << params.valodityHomographySigma;
	std::cout << "/" << params.valodityHomographySigmaF << "\n";
	std::cout << "  - STD OF: " << params.stdDistributionSigma;
	std::cout << "/" << params.stdDistributionSigmaF << "\n";
	std::cout << "  - BH distance: " << params.bhDistanceSigma;
	std::cout << "/" << params.bhDistanceSigmaF << "\n";
	std::cout << std::endl;
}


/*
 * MAIN
 */

int main(int argc, char *argv[])
{
	// Manage arguments
	if (argc != 2)
	{
		usage();
		return 0;
	}

	// Load data
	std::list<DataPoint> data;

	if (!loadData(data, argv[1]))
	{
		std::cout << "Error reading input file" << std::endl;
		return 0;
	}

	// Print data for debug
#ifdef DEBUG
	printData(data);
#endif

	// Init Bayesian network
	BNetwork bNet;
	BNetParams bNetParams = {
		400, 360.0,
		133.0, 4.0,
		15.0, 30.0,
		0.01, 0.6,
		15.0, 150.0,
		10.0, 60.0};
	bNet.init_params(
		bNetParams.nFeatures_max,
		bNetParams.distribution_std_max);
	bNet.init_network(
		bNetParams.nFeatSigma, bNetParams.nFeatSigmaF,
		bNetParams.percLostFeatSigma, bNetParams.percLostFeatSigmaF,
		bNetParams.valodityHomographySigma, bNetParams.valodityHomographySigmaF,
		bNetParams.stdDistributionSigma, bNetParams.stdDistributionSigmaF,
		bNetParams.bhDistanceSigma, bNetParams.bhDistanceSigmaF);

	// Evaluate network
	std::cout << "Base score = " << evaluateBNet(data, bNet, bNetParams) << "\n";
	printParams(bNetParams);

	// Parameter range
	BNetParams bNetParamsMin = {
		400, 360.0,
		100.0, 4.0,
		5.0, 15.0,
		0.01, 0.4,
		5.0, 50.0,
		0.0, 30.0};
	BNetParams bNetParamsMax = {
		400, 360.0,
		200.0, 8.0,
		30.0, 60.0,
		0.5, 0.8,
		45.0, 300.0,
		50.0, 100.0};

	// Montecarlo sampling
	int nIterations = 1000;
	double score;
	double bestScore = 0.0;
	BNetParams bestParams = bNetParams;
	gsl_rng *rng = gsl_rng_alloc(gsl_rng_taus);
#ifdef RNG_SEED
	gsl_rng_set(rng, RNG_SEED);
#else
	gsl_rng_set(rng, time(NULL));
#endif

	for (int i = 0; i < nIterations; i++)
	{
		// Set random parameters
		setRandomParams(rng, bNetParams, bNetParamsMin, bNetParamsMax);

		// Evaluate network
		score = evaluateBNet(data, bNet, bNetParams);

		std::cout << "Iteration " << i << ", score = " << score << "\n";

		if (score > bestScore)
		{
			bestScore = score;
			bestParams = bNetParams;
			std::cout << "--------------- BEST! ----------------\n";
		}

		printParams(bNetParams);
	}

	score = evaluateBNet(data, bNet, bestParams);
	std::cout << "--------------- RESULT ----------------\n";
	std::cout << "Score = " << score << "\n";
	std::cout << "  Fail detection weight: " << SCORE_FAIL << "\n";
	std::cout << "  Non-fail detection weight: " << SCORE_NON_FAIL << "\n";
	std::cout << "Parameters:\n";
	printParams(bestParams);

#ifdef DEBUG
	printData(data);
#endif


	// Exit
	return 0;
}
