#include <tracking_evaluation.h>
#define VERBOSE

int main(int argc, char* argv[])
{

	if(argc != 3)
	{
		std::cout << "Insert: image_path - frames_number"<< std::endl;
		return 0;
	}

	TrackingEvaluation eval;

	cv::Size img_size;
	std::string path_folder = argv[1];
	int n_frames = atoi(argv[2]);

	eval.set_image_size(
			path_folder.substr(0, path_folder.size() - 11),
			img_size);
#ifdef VERBOSE
	std::cout << "n_frames: " << n_frames << std::endl;
	std::cout << "path_folder: " << path_folder << std::endl;
	std::cout << "image size: " << img_size << std::endl;
#endif

	// Declare variables
	cv::Mat GT_contours[n_frames+2];
	cv::Mat TK_contours[n_frames];
	cv::Mat TK_Ncontours;
	cv::Mat Attributes;
	cv::Mat img_GT;
	cv::Mat img_TK;
	cv::Mat img_intersection;
	int n_intersection = 0;
	int n_union = 0;
	cv::Mat overlap_ratio;
	cv::Mat prova(img_size, CV_8UC3);
	overlap_ratio.create(n_frames,1, CV_32FC1);

	cv::namedWindow("prova", cv::WINDOW_AUTOSIZE);
	cv::namedWindow("GT", cv::WINDOW_AUTOSIZE);
	cv::namedWindow("TK", cv::WINDOW_AUTOSIZE);
	cv::namedWindow("inter", cv::WINDOW_AUTOSIZE);

	// create Mat
	img_GT.create(img_size, CV_8UC1);
	img_TK.create(img_size, CV_8UC1);
	img_intersection.create(img_size, CV_8UC1);

	// initialize
	img_GT.setTo(0);
	img_TK.setTo(0);
	img_intersection.setTo(0);

	cv::Vec3b red(255,0,0);
	cv::Vec3b blue(0,0,255);


	std::vector<cv::Point> contourGT;
	std::vector<cv::Point> contourTK;
	std::vector<std::vector<cv::Point> > contourTK_PO_;
	std::vector<std::vector<std::vector<cv::Point> > >contourTK_PO;

	bool adap_partial_occlusion = true;
	int n_contours;



	// Declare paths
	std::string GT_file_name = path_folder.substr(0, path_folder.size() - 11) + "/data/GT.xml";
	std::string TK_file_name = path_folder.substr(0, path_folder.size() - 11) + "/data/TK.xml";
	std::string TK_PO_file_name = path_folder.substr(0, path_folder.size() - 11) + "/data/TK_PO.xml";
	std::string oRatio_file_name = path_folder.substr(0, path_folder.size() - 11) + "/data/OverlapRatio.xml";
	std::string att_file_name = path_folder.substr(0, path_folder.size() - 11) + "/data/Attributes.xml";
	std::string NContour_file_name = path_folder.substr(0, path_folder.size() - 11) + "/data/NContour.xml";

#ifdef VERBOSE
	std::cout << "saving path_folder: " << GT_file_name << std::endl;
	std::cout << "saving path_folder: " << TK_file_name << std::endl;
	std::cout << "saving path_folder: " << TK_PO_file_name << std::endl;
	std::cout << "saving path_folder: " << oRatio_file_name << std::endl;
	std::cout << "saving path_folder: " << att_file_name << std::endl;
	std::cout << "saving path_folder: " << NContour_file_name << std::endl;
#endif

	// load the GT contour
	eval.load_GTcontours(GT_file_name,
			n_frames,
			GT_contours);

	// load attributes
	eval.load_Attributes(
			att_file_name,
			n_frames,
			Attributes);

	if(!adap_partial_occlusion)
	{

		// load the tracked TK contour
		eval.load_TKcontours(TK_file_name,
				n_frames,
				TK_contours);
	}
	else
	{

		// load number of contour for each frames
		eval.load_NContours(
				NContour_file_name,
				n_frames,
				TK_Ncontours);

		int n_contours = 0;
		// load TK adapted for partial occlusion
		for(int i = 0; i < TK_Ncontours.rows; i++)
		{
			if(TK_Ncontours.at<int>(i,0) > 0)
			{
				n_contours = n_contours + TK_Ncontours.at<int>(i,0);

			}
			else
			{
				n_contours = n_contours + 1;

			}

		}

		std::cout << "Total Number of Contours: " <<  n_contours<< std::endl;

		cv::FileStorage fsTK_PO;
		char varName[64];
		std::cout << "reading: " << TK_PO_file_name << std::endl;
		fsTK_PO.open(TK_PO_file_name, cv::FileStorage::READ);


		cv::Mat contour_temp;
		int k = 0;
		int k_2 = 0;

		contourTK_PO.clear();
		for(int i = 0; i < n_frames; i++)
		{
			// store n_contours and correspondent contours
			std::cout << "N frames saving: " << i << " ....." << std::endl;
			int n_c = TK_Ncontours.at<int>(i,0);
			std::cout << "n_c: " << n_c << std::endl;
			contourTK_PO_.clear();
			if (n_c == 0)
			{
				n_c = 1;
			}

			for(int j = 0; j < n_c; j++)
			{

				if(n_c == 1)
				{

					sprintf(varName,"TK_PO%06d", i);
					//					std::cout << (std::string)varName << std::endl;
					//k_2 = n_c;
				}
				else if(n_c > 1){

					sprintf(varName, "TK_PO%06d%c", i, 'a' + j);
					//					std::cout << (std::string)varName << std::endl;
					//k_2 = n_c -1;
				}

				fsTK_PO[varName] >> contour_temp;

				std::vector<cv::Point> temp_vec;
				for(int m = 0; m < contour_temp.rows; m++)
				{
					cv::Point temp_p;

					temp_p.x = contour_temp.at<float>(m,0);
					temp_p.y = contour_temp.at<float>(m,1);
					temp_vec.push_back(temp_p);

				}
				contourTK_PO_.push_back(temp_vec);
			}

			//k = k +  k_2 ;
			contourTK_PO.push_back(contourTK_PO_);
		}

	}



	// compute overlap ratio
	for(int i = 0; i < n_frames; i++)
	{
		prova.setTo(0);
		img_GT.setTo(0);
		img_TK.setTo(0);
		img_intersection.setTo(0);

		char varName[64];
		sprintf(varName,"imL%06d", i);
		std::string im_path = path_folder.substr(0, path_folder.size() - 11) + varName + ".png";
		std::cout << "N frames: " << i << " ....." << std::endl;
		//		std::cout << im_path << std::endl;
		prova = cv::imread(im_path);

		//		std::cout << prova << std::endl;
#ifdef VERBOSE

		std::cout << "TK: " << TK_contours[i] << std::endl;
		std::cout << "GT: " <<  GT_contours[i] << std::endl;
		//		std::cout << "Att: " <<  Attributes << std::endl;

#endif
		contourGT.clear();
		contourTK.clear();

		// save contour GT
		for(int j = 0; j < GT_contours[i].rows; j++)
		{
			cv::Point temp;
			temp.x = GT_contours[i].at<float>(j,0);
			temp.y = GT_contours[i].at<float>(j,1);
			contourGT.push_back(temp);
		}

		if(!adap_partial_occlusion)
		{
			// save contour TK
			for(int j = 0; j < TK_contours[i].rows; j++)
			{
				cv::Point temp;
				temp.x = TK_contours[i].at<float>(j,0);
				temp.y = TK_contours[i].at<float>(j,1);
				contourTK.push_back(temp);
			}
		}

		if(adap_partial_occlusion)
		{

			std::cout << "GT " << contourGT.empty() << std::endl;
			std::cout << "Attributes " << (int)Attributes.at<uchar>(i,0) << std::endl;
			std::cout << "contourTK_PO " << contourTK_PO[i][0][0].x << std::endl;

			//	overlap only if GT is present
			if(!contourGT.empty() && Attributes.at<uchar>(i,0) != 2 && Attributes.at<uchar>(i,0)!= 3 &&
					contourTK_PO[i][0][0].x > 0 && contourTK_PO[i][0][0].y > 0)
			{

				// False Positive or True Positive depending on the threshold value

				// create image labeled with the GT contour
				cv::fillConvexPoly(img_GT, contourGT, cv::Scalar(1));
				//	 cv::drawContours(img_intersection, contour ,-1, cv::Scalar(1), -1);

				int n_c = TK_Ncontours.at<int>(i,0);
				for(int j = 0; j < n_c; j++)
				{
					//					std::cout << "TK" << cv::Mat(contourTK_PO[i][j]) << std::endl;
					// create image labeled with the TK contour
					cv::fillConvexPoly(img_TK, contourTK_PO[i][j], cv::Scalar(1));
					//					std::cout << "contourTK_PO " << contourTK_PO[i][j]<< std::endl;
				}

				// compute intersection
				cv::bitwise_and(img_GT, img_TK, img_intersection);


				cv::imshow("GT", img_GT);


				cv::imshow("TK", img_TK);


				cv::imshow("inter", img_intersection);



				// count number of non_zero element
				n_intersection = countNonZero(img_intersection);

				// compute union
				n_union = countNonZero(img_GT) + countNonZero(img_TK) - n_intersection;

				// compute overlap ratio
				overlap_ratio.at<float>(i,0) = (double)n_intersection/(double)n_union ;

				std::cout << overlap_ratio.at<float>(i,0) << std::endl;

				for(int i = 0; i < img_size.height; i++)
				{
					for(int j = 0; j < img_size.width; j++)
					{
						if(img_GT.at<uchar>(i,j) == 1)
						{
							prova.at<cv::Vec3b>(i,j) = red;
						}
						if(img_TK.at<uchar>(i,j) == 1)
						{
							prova.at<cv::Vec3b>(i,j) = blue;
						}
					}
				}
			}
			else if(contourTK_PO[i][0][0].x == -1 && contourTK_PO[i][0][0].y == -1 &&
					Attributes.at<uchar>(i,0) != 2 && Attributes.at<uchar>(i,0)!= 3
					&& !contourGT.empty())
			{

				prova = cv::imread(im_path);				// false negative FN
				overlap_ratio.at<float>(i,0) = -2;
			}
			else if(contourGT.empty() && contourTK_PO[i][0][0].x == -1 && contourTK_PO[i][0][0].y == -1)
			{

				prova = cv::imread(im_path);				// true negative TN
				// true negative TN
				overlap_ratio.at<float>(i,0) = -1;
			}
			else
			{
				prova = cv::imread(im_path);				// false positive FP
				// true negative TN
				overlap_ratio.at<float>(i,0) = -3;
			}
			std::cout << "n frame" << i << std::endl;
			cv::imshow("prova", prova);
			cv::waitKey(40);
		}
		else
		{
			//	overlap only if GT is present
			if(!contourGT.empty() && Attributes.at<uchar>(i,0) != 2 && Attributes.at<uchar>(i,0)!= 3 &&
					TK_contours[i].at<float>(0,0) > 0)
			{
				// False Positive or True Positive depending on the threshold value
				// create image labeled with the GT contour
				cv::fillConvexPoly(img_GT, contourGT, cv::Scalar(1));
				//	 cv::drawContours(img_intersection, contour ,-1, cv::Scalar(1), -1);
				// create image labeled with the TK contour
				cv::fillConvexPoly(img_TK, contourTK, cv::Scalar(1));
				// compute intersection
				cv::bitwise_and(img_GT, img_TK, img_intersection);
				// count number of non_zero element
				n_intersection = countNonZero(img_intersection);
				// compute union
				n_union = countNonZero(img_GT) + countNonZero(img_TK) - n_intersection;
				// compute overlap ratio
				overlap_ratio.at<float>(i,0) = (double)n_intersection/(double)n_union ;

				std::cout << overlap_ratio.at<float>(i,0) << std::endl;


				for(int i = 0; i < img_size.height; i++)
				{
					for(int j = 0; j < img_size.width; j++)
					{
						if(img_GT.at<uchar>(i,j) == 1)
						{
							prova.at<cv::Vec3b>(i,j) = red;
						}
						if(img_TK.at<uchar>(i,j) == 1)
						{
							prova.at<cv::Vec3b>(i,j) = blue;
						}
					}
				}
			}
			else if(TK_contours[i].at<float>(0,0) == -1 &&
					Attributes.at<uchar>(i,0) != 2 && Attributes.at<uchar>(i,0)!= 3
					&& !contourGT.empty())
			{

				prova = cv::imread(im_path);
				// false negative FN
				overlap_ratio.at<float>(i,0) = -2;
			}
			else if(contourGT.empty() && TK_contours[i].at<float>(0,0) == -1)
			{
				std::cout << "GT " << contourGT.empty() << std::endl;
				prova = cv::imread(im_path);
				// true negative TN
				overlap_ratio.at<float>(i,0) = -1;
			}
			else
			{
				prova = cv::imread(im_path);
				// true negative TN
				overlap_ratio.at<float>(i,0) = -3;
			}
			std::cout << "n frame" << i << std::endl;
			cv::imshow("prova", prova);
			cv::waitKey(40);
		}
	}

	std::cout << "Saving Overlap!" << std::endl;

	// save overlap ration
	eval.save_overlap_ratio(
			oRatio_file_name,
			overlap_ratio);
}
