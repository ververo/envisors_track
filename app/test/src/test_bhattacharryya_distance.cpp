#include <iostream>
#include <stdlib.h>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <math.h>
#include "opencv2/core/types.hpp"
#include "opencv2/features2d/features2d.hpp"


void load_SafetArea(
		std::string file_name,
		std::vector <std::vector<cv::Point2f> > &safety_area,
		int nframes)
{
	safety_area.resize(nframes);

	cv::FileStorage fsGT;
	char varName[32];
	fsGT.open(file_name, cv::FileStorage::READ);
	cv::Mat tempROI;

	if (!fsGT.isOpened())
	{
		std::cout << "Failed to open "  << std::endl;
		return;
	}
	else{
		for(int i = 0; i < nframes; i++)
		{
			sprintf(varName,"GT%06d", i);
			fsGT[varName] >> tempROI;
			if(tempROI.rows > 0)
			{
				for(int j = 0; j < tempROI.rows; j++)
				{
					cv::Point2f p;
					p.x = tempROI.at<float>(j, 0);
					p.y = tempROI.at<float>(j, 1);
					safety_area[i].push_back(p);
				}
			}
			else
			{
				//				cv::Point2f p;
				//				p.x = -1;
				//				p.y = -1;
				//				safety_area[i].push_back(p);
			}

		}
	}

	fsGT.release();
}

// this function compute normalized hsv histogram as a bidimensional combination of s and h channel
void hsv_image_histogram(
		const cv::Mat& hsv_img,
		cv::Mat& hist_asv,
		std::vector<cv::Point2f> &safety_area,
		int n_hbin,
		int n_sbin)
{
	int num_bin;
	float h_step;
	float s_step;
	float h;
	float s;
	int h_idx;
	int s_idx;
	int bin_idx;

	num_bin = n_hbin * n_sbin;

	h_step = 180.0 / n_hbin;
	s_step = 256.0 / n_sbin;
	hist_asv.create(n_hbin*n_sbin, 1, CV_32FC1);

	hist_asv.setTo(0);

	// compute rectangular area around the SA
	bool totallyOut = false;
	cv::Rect rect_SA = boundingRect(safety_area);
	if (rect_SA.x < 0)
		rect_SA.x = 0;
	if (rect_SA.x >= hsv_img.cols)
		totallyOut = true;
	if (rect_SA.x + rect_SA.width >= hsv_img.cols)
		rect_SA.width = hsv_img.cols - rect_SA.x -1;

	if (rect_SA.y < 0)
		rect_SA.y = 0;
	if (rect_SA.y >= hsv_img.rows)
		totallyOut = true;
	if (rect_SA.y + rect_SA.height >= hsv_img.rows)
		rect_SA.height = hsv_img.rows - rect_SA.y -1;

	if(!totallyOut){
		int count_nhist = 0;
		std::vector<cv::KeyPoint> temp;

		for(int row = rect_SA.y; row < rect_SA.y + rect_SA.height; row++)
		{

			for(int col = rect_SA.x; col < rect_SA.x + rect_SA.width; col++)
			{
				cv::Point p_temp;
				p_temp.x = col;
				p_temp.y = row;

				if (cv::pointPolygonTest(safety_area, p_temp, false) < 0)
					continue;

				// The point is inside the safety area
				cv::Vec3b pixel = hsv_img.at<cv::Vec3b>(row, col);
				// get bin idx
				h = pixel[0];
				s = pixel[1];


				if(h > 180.0)
					h = 180.0;

				if(s > 256.0)
					s = 256.0;

				h_idx = int(h/h_step);
				s_idx = int(s/s_step);


				if (h_idx >= n_hbin || s_idx >= n_sbin)
				{

					continue;
				}

				bin_idx = h_idx + s_idx * n_hbin;

				hist_asv.at<float>(bin_idx, 0)++;
				count_nhist++;
			}
		}

		//	cv:drawKeypoints(img_out, temp, cv::Mat(), 0.5 );

		for (int i = 0; i < hist_asv.rows; i++)
		{
			if (count_nhist != 0){
				hist_asv.at<float>(i, 0) = hist_asv.at<float>(i, 0) / count_nhist;
			}
		}
	}
}

void compute_BhattaCharryya_distance(
		cv::Mat& hsv_hist_1,
		cv::Mat& hsv_hist_2,
		double &bh_distance)
{

	double ro_h1_h2 = 0.0;

	// check if hist_1 and hist_2 have the same size
	if(hsv_hist_1.rows == hsv_hist_2.rows)
	{
		for(int i = 0; i < hsv_hist_1.rows; i++)
		{
			ro_h1_h2 = ro_h1_h2 + sqrt(hsv_hist_1.at<float>(i, 0)*hsv_hist_2.at<float>(i, 0));
		}

		if (ro_h1_h2 >= 1.0)
			bh_distance = 0.0;
		else
			bh_distance = sqrt(1.0 - ro_h1_h2);

		std::cout << "BhattaCharryya distance: " << bh_distance << std::endl;

	}
	else
	{
		bh_distance = 1.0;
		std::cout << "hist_1 and hist_2 have to have the same size" << std::endl;
	}
}




int main(int argc, char **argv) {

	double prova_bh_distance = 0.0;
	std::vector<std::vector<cv::Point2f> > safety_areas;
	cv::Mat img_1;
	cv::Mat img_2;
	cv::Mat imgHSV_1;
	cv::Mat imgHSV_2;
	cv::Mat hist_1;
	cv::Mat hist_2;
	int nframes = 7000;
	char varName[64];
	std::string im_path = "/home/veronica/PhD/ros_workspace/dataset/stereo_pairs/tracking/vessel-3/";
	std::string data_path = "/home/veronica/PhD/ros_workspace/dataset/stereo_pairs/tracking/kidney_1/";
	cv::Mat model_ref;
	cv::Mat model_curr;
	cv::Rect templateROI;

	// create windows to comapre models
	cv::namedWindow("model ref", CV_WINDOW_AUTOSIZE);
	cv::namedWindow("model curr", CV_WINDOW_AUTOSIZE);
	// load safety areas
	load_SafetArea(
			data_path+"data/GT.xml",
			safety_areas,
			nframes);
	// read reference image
	img_1 = cv::imread(im_path + "imL000002.png");
	// create refreence model template for visualization
	templateROI = boundingRect(safety_areas[2]);
	model_ref = img_1(templateROI);
	// compute reference histogram
	cv::cvtColor(img_1, imgHSV_1, cv::COLOR_BGR2HSV);
	// compute histogram1
	hsv_image_histogram(
			imgHSV_1,
			hist_1,
			safety_areas[2],
			10, 10);
	// compute Bhattaccharryya distance for every frames
	for(int i = 2; i < nframes; i++)
	{
		// only if there is safety area ...
		if(!safety_areas[i].empty()){

//			std::cout << "N frames: " << i << " ....." << std::endl;
			// read the frame
			sprintf(varName,"imL%06d.png", i);
			img_2 = cv::imread(im_path + varName );
			// for debug...
			//			std::cout << safety_areas[i].size() << std::endl;
			//			cv::fillConvexPoly(img_out, safety_areas[i], cv::Scalar(1,0,0));
			// create current model template for visualization
			bool totallyOut = false;
			templateROI = boundingRect(safety_areas[i]);
			if (templateROI.x < 0)
				templateROI.x = 0;
			if (templateROI.x >= img_2.cols)
				totallyOut = true;
			if (templateROI.x + templateROI.width >= img_2.cols)
				templateROI.width = img_2.cols - templateROI.x -1;

			if (templateROI.y < 0)
				templateROI.y = 0;
			if (templateROI.y >= img_2.rows)
				totallyOut = true;
			if (templateROI.y + templateROI.height >= img_2.rows)
				templateROI.height = img_2.rows - templateROI.y -1;

			if(!totallyOut && templateROI.height >= 4 && templateROI.width >= 4)
			{
				model_curr = img_2(templateROI);
			}

			cv::imshow("model ref",model_ref);
			cv::imshow("model curr",model_curr);

//			cv::waitKey();
			// convert image from RGB to HSV
			cv::cvtColor(img_2, imgHSV_2, cv::COLOR_BGR2HSV);
			// compute histogram2
			hsv_image_histogram(
					imgHSV_2,
					hist_2,
					safety_areas[i],
					10, 10);
			// compute distance
			compute_BhattaCharryya_distance(
					hist_1, hist_2,
					prova_bh_distance);
		}
		else
		{
			prova_bh_distance = -1;
			std::cout << "BhattaCharryya distance: " << prova_bh_distance << std::endl;
		}
	}
	return 0;
}

