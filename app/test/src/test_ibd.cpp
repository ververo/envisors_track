#include <iostream>
#include <stdlib.h>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <math.h>
#include "opencv2/core/types.hpp"
#include "opencv2/features2d/features2d.hpp"


void load_Attributes(
		std::vector <int> &attributes,
		std::string file_name,
		int nframes)
{
	cv::FileStorage fsAtt;
	char varName[32];
	fsAtt.open(file_name, cv::FileStorage::READ);
	cv::Mat tempROI;

	attributes.resize(nframes);

	if (!fsAtt.isOpened())
	{
		std::cout << "Failed to open "  << std::endl;
		return;
	}
	else{

		cv::Mat att;
		fsAtt["Attributes"] >> att;
		std::cout << att.rows << std::endl;


		for(int i = 0; i < nframes; i++)
		{
			attributes[i] = att.at<uchar>(i,0);
			std::cout << "att: " << (int)att.at<uchar>(i,0) << std::endl;
		}
	}
	fsAtt.release();
}



void load_SafetArea(
		std::string file_name,
		std::vector <std::vector<cv::Point2f> > &safety_area,
		int nframes)
{
	safety_area.resize(nframes);

	cv::FileStorage fsGT;
	char varName[32];
	fsGT.open(file_name, cv::FileStorage::READ);
	cv::Mat tempROI;

	if (!fsGT.isOpened())
	{
		std::cout << "Failed to open "  << std::endl;
		return;
	}
	else{
		for(int i = 0; i < nframes; i++)
		{
			sprintf(varName,"GT%06d", i);
			fsGT[varName] >> tempROI;
			if(tempROI.rows > 0)
			{
				for(int j = 0; j < tempROI.rows; j++)
				{
					cv::Point2f p;
					p.x = tempROI.at<float>(j, 0);
					p.y = tempROI.at<float>(j, 1);
					safety_area[i].push_back(p);
				}
			}
			else
			{
				//				cv::Point2f p;
				//				p.x = -1;
				//				p.y = -1;
				//				safety_area[i].push_back(p);
			}

		}
	}

	fsGT.release();
}

// detect if a frame is blurr
float detectBlurredFrame(
		cv::Mat& img)
{
	//variables
	cv::Mat hsl_image, F;
	std::vector<cv::Mat> hls_channels(3);
	double m, M;
	int n_kern_x = 9, n_kern_y = 1;
	cv::Mat Bver, Bhor;
	cv::Mat h_kernel;
	cv::Mat v_kernel;
	cv::Mat Vver, Vhor;
	cv::Rect r;
	cv::Mat D_Fver_no_b, D_Fhor_no_b, Vver_no_b, Vhor_no_b;

	// split image in HLS channel
	cv::cvtColor(img, hsl_image, cv::COLOR_RGB2HLS);     //Convert from RGB to HLS

	// take only the luminance channel
	split(hsl_image, hls_channels);
	F = hls_channels[1]; //luminance

	// Normalization between 0 and 255
	minMaxLoc(F, &m, &M);
	F.convertTo(F, CV_32F, 255./(M-m), -m * 255.0/(M - m));

	//CRETE
	h_kernel =  1.0/n_kern_x * cv::Mat::ones(cv::Size(n_kern_x, n_kern_y), CV_32F);   //h_kernel = 1/9*[1 1 1 1 1 1 1 1 1 ]-->smussa verticalmente

	transpose(h_kernel, v_kernel);
	//cout << h_kernel << endl;

	//non flippo perchè il kernel è simm--> corr = conv
	filter2D(F, Bver, -1, h_kernel);  //h_kernel = 1/9*[1 1 1 1 1 1 1 1 1 ]-->smussa verticalmente
	filter2D(F, Bhor, -1, v_kernel);

	//finite differences
	cv::Mat D_Fver = cv::Mat::zeros(F.size(), CV_32F);
	cv::Mat D_Fhor = cv::Mat::zeros(F.size(), CV_32F);
	cv::Mat D_Bver = cv::Mat::zeros(F.size(), CV_32F);
	cv::Mat D_Bhor = cv::Mat::zeros(F.size(), CV_32F);

	// find absolute differences on row and line of sharp and blurred images
	for (int i = 1; i < img.rows-2; i++){
		for (int j = 1; j < img.cols-2; j++){

			D_Fver.at<float>(i,j) = abs(F.at<float>(i,j) - F.at<float>(i-1,j));
			D_Fhor.at<float>(i,j) = abs(F.at<float>(i,j) - F.at<float>(i,j-1));
			D_Bver.at<float>(i,j) = abs(Bver.at<float>(i,j) - Bver.at<float>(i-1,j));
			D_Bhor.at<float>(i,j) = abs(Bhor.at<float>(i,j) - Bhor.at<float>(i,j-1));
		}
	}


	// take only the absolute difference that are decreased (> 0)
	Vver = cv::max(0, D_Fver - D_Bver);
	Vhor = cv::max(0, D_Fhor - D_Bhor);

	//remove borders
	r.y = 1;
	r.x = 1;
	r.width = F.cols-2;
	r.height = F.rows -2;

	D_Fver_no_b = D_Fver(r);
	D_Fhor_no_b = D_Fhor(r);
	Vver_no_b = Vver(r);
	Vhor_no_b = Vhor(r);

	// sum the differences
	double s_Fver = cv::sum( D_Fver_no_b )[0];
	double s_Fhor = cv::sum( D_Fhor_no_b )[0];
	double s_Vver = cv::sum( Vver_no_b )[0];
	double s_Vhor = cv::sum( Vhor_no_b )[0];


	// compute the blurrign value and normalize it
	float b_Fver = float(s_Fver-s_Vver)/s_Fver;  //0-1
	float b_Fhor = float(s_Fhor-s_Vhor)/s_Fhor; //0-1

	// find the higher component in x and y
	float blur_F = cv::max(b_Fver, b_Fhor);
	std::cout << "blur: "  << blur_F << std::endl;

	// compute image blurred

	return blur_F;

}




int main(int argc, char **argv) {

	float blur_value = 0.0;
	std::vector<std::vector<cv::Point2f> > safety_areas;
	std::vector<int> att;
	cv::Mat img_1;
	cv::Mat img_2;
	int nframes = 1500;
	char varName[64];
	std::string im_path = "/home/veronica/PhD/ros_workspace/dataset/stereo_pairs/tracking/vessel-3/";
	std::string data_path = "/home/veronica/PhD/ros_workspace/dataset/stereo_pairs/tracking/vessel-3/";

	cv::Mat model_curr;
	cv::Rect templateROI;

	// create windows to comapre models
	cv::namedWindow("model curr", CV_WINDOW_AUTOSIZE);

	// load safety areas
	load_SafetArea(
			data_path+"data/GT.xml",
			safety_areas,
			nframes);

	load_Attributes(
			att,
			data_path+"data/Attributes_blur.xml",
			nframes+2);

	float th[10];
	int tp[10] = {0,0,0,0,0,0,0,0,0,0};
	int fp[10] = {0,0,0,0,0,0,0,0,0,0};
	int tn[10] = {0,0,0,0,0,0,0,0,0,0};
	int fn[10] = {0,0,0,0,0,0,0,0,0,0};

	th[0] = 0.1;
	th[1] = 0.2;
	th[2] = 0.3;
	th[3] = 0.4;
	th[4] = 0.5;
	th[5] = 0.6;
	th[6] = 0.7;
	th[7] = 0.8;
	th[8] = 0.9;
	th[9] = 1.0;

	bool estimated_blur = false;
	bool blur = false;

	// detect if a frame is blurred
	for(int i = 2; i < nframes; i++)
	{
		// only if there is safety area ...
		if(!safety_areas[i].empty()){

			//			std::cout << "N frames: " << i << " ....." << std::endl;
			// read the frame
			sprintf(varName,"imL%06d.png", i);
			img_2 = cv::imread(im_path + varName );
			// for debug...
			//			std::cout << safety_areas[i].size() << std::endl;
			//			cv::fillConvexPoly(img_out, safety_areas[i], cv::Scalar(1,0,0));
			// create current model template for visualization
			bool totallyOut = false;
			templateROI = boundingRect(safety_areas[i]);
			if (templateROI.x < 0)
				templateROI.x = 0;
			if (templateROI.x >= img_2.cols)
				totallyOut = true;
			if (templateROI.x + templateROI.width >= img_2.cols)
				templateROI.width = img_2.cols - templateROI.x -1;

			if (templateROI.y < 0)
				templateROI.y = 0;
			if (templateROI.y >= img_2.rows)
				totallyOut = true;
			if (templateROI.y + templateROI.height >= img_2.rows)
				templateROI.height = img_2.rows - templateROI.y -1;

			if(!totallyOut && templateROI.height >= 4 && templateROI.width >= 4)
			{
				model_curr = img_2(templateROI);
			}

			cv::imshow("model curr",model_curr);
			//			cv::waitKey();

			blur_value =  detectBlurredFrame(
					model_curr);


			blur = (att[i+2] == 1);

			for (int i = 0; i < 10; i++)
			{
				estimated_blur = (blur_value > th[i]);

				if(estimated_blur && blur)
					tp[i]++;
				else if(estimated_blur && !blur)
					fp[i]++;
				else if(!estimated_blur && blur)
					fn[i]++;
				else if(!estimated_blur && !blur)
					tn[i]++;
			}

		}
		else
		{
			blur_value = -1;
			std::cout << "blur: "  << blur_value << std::endl;
		}


	}
	for (int i = 0; i < 10; i++)
	{
		std::cout << tp[i] << "," << fp[i] << "," << tn[i]  << "," << fn[i] << std::endl;
	}
	return 0;
}

