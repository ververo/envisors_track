#include <gt_generator.h>


void callbackButton_PO(int state,  void* data)
{
	std::cout << "Partial Occlusion" << std::endl;

	// convert userdata to the right type
	GTGenerator *ptr = ((GTGenerator*)(data));

	if (!ptr)
	{
		std::cout << "Partial Occlusion empty" << std::endl;
		ptr->GT[ptr->n_frame].attribute = 0;
		return;
	}
	else
	{
		ptr->GT[ptr->n_frame].attribute = 1;
		ptr->refresh_ROI();
		//cv::putText(ptr->img, "Partial Occlusion", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
		//		4.0, cv::Scalar(255,0,0), 2);
		cv::imshow("Ground Truth Generation", ptr->img);
	}
}

void callbackButton_TO(int state,  void* data)
{
	std::cout << "Total Occlusion" << std::endl;

	// convert userdata to the right type
	GTGenerator *ptr = ((GTGenerator*)(data));

	if (!ptr)
	{
		std::cout << "Total Occlusion is empty" << std::endl;
		ptr->GT[ptr->n_frame].attribute = 0;
		return;
	}
	else
	{
		ptr->GT[ptr->n_frame].attribute = 2;
		ptr->refresh_ROI();
		//cv::putText(ptr->img, "Out of field", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
		//		4.0, cv::Scalar(255,0,0), 2);
		cv::imshow("Ground Truth Generation", ptr->img);

	}
}

void callbackButton_OFF(int state,  void* data)
{
	std::cout << "Out of field of view" << std::endl;

	// convert userdata to the right type
	GTGenerator *ptr = ((GTGenerator*)(data));

	if (!ptr)
	{
		std::cout << "Out of field of view is empty" << std::endl;
		ptr->GT[ptr->n_frame].attribute = 0;
		return;
	}
	else
	{
		ptr->GT[ptr->n_frame].attribute = 3;
		ptr->refresh_ROI();
		//cv::putText(ptr->img, "Total Occlusion", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
		//		4.0, cv::Scalar(255,0,0), 2);
		cv::imshow("Ground Truth Generation", ptr->img);
	}
}

void callbackButton_DEF(int state,  void* data)
{
	std::cout << "Deformation" << std::endl;

	// convert userdata to the right type
	GTGenerator *ptr = ((GTGenerator*)(data));

	if (!ptr)
	{
		std::cout << "Deformation is empty" << std::endl;
		ptr->GT[ptr->n_frame].attribute = 0;
		return;
	}
	else
	{
		ptr->GT[ptr->n_frame].attribute = 4;
		ptr->refresh_ROI();
		//cv::putText(ptr->img, "Deformation", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
		//		4.0, cv::Scalar(255,0,0), 2);
		cv::imshow("Ground Truth Generation", ptr->img);
	}
}

void callbackButton_IL(int state,  void* data)
{
	std::cout << "Illumination changes" << std::endl;

	// convert userdata to the right type
	GTGenerator *ptr = ((GTGenerator*)(data));

	if (!ptr)
	{
		std::cout << "Illumination is empty" << std::endl;
		ptr->GT[ptr->n_frame].attribute = 0;
		return;
	}
	else
	{
		ptr->GT[ptr->n_frame].attribute = 5;
		ptr->refresh_ROI();
		//cv::putText(ptr->img, "Illumination changes", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
		//		4.0, cv::Scalar(255,0,0), 2);
		cv::imshow("Ground Truth Generation", ptr->img);
	}
}

void callbackButton_CAM(int state,  void* data)
{
	std::cout << "Abrupt camera motion" << std::endl;

	// convert userdata to the right type
	GTGenerator *ptr = ((GTGenerator*)(data));

	if (!ptr)
	{
		std::cout << "Camera is empty" << std::endl;
		ptr->GT[ptr->n_frame].attribute = 0;
		return;
	}
	else
	{
		ptr->GT[ptr->n_frame].attribute = 6;
		ptr->refresh_ROI();
		//cv::putText(ptr->img, "Abrupt camera motion", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
		//		4.0, cv::Scalar(255,0,0), 2);
		cv::imshow("Ground Truth Generation", ptr->img);
	}
}

void callbackButton_BLUR(int state,  void* data)
{
	std::cout << "Blur" << std::endl;

	// convert userdata to the right type
	GTGenerator *ptr = ((GTGenerator*)(data));

	if (!ptr)
	{
		std::cout << "Blur is empty" << std::endl;
		ptr->GT[ptr->n_frame].attribute = 0;
		return;
	}
	else
	{
		ptr->GT[ptr->n_frame].attribute = 7;
		ptr->refresh_ROI();
		//cv::putText(ptr->img, "Blur", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
		//		4.0, cv::Scalar(255,0,0), 2);
		cv::imshow("Ground Truth Generation", ptr->img);
	}
}

void callbackButton_SMOKE(int state,  void* data)
{
	std::cout << "Smoke" << std::endl;

	// convert userdata to the right type
	GTGenerator *ptr = ((GTGenerator*)(data));

	if (!ptr)
	{
		std::cout << "Smoke is empty" << std::endl;
		ptr->GT[ptr->n_frame].attribute = 0;
		return;
	}
	else
	{
		ptr->GT[ptr->n_frame].attribute = 8;
		ptr->refresh_ROI();
		//cv::putText(ptr->img, "Smoke", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
		//		4.0, cv::Scalar(255,0,0), 2);
		cv::imshow("Ground Truth Generation", ptr->img);
	}
}

int main(int argc, char* argv[])
{

	if(argc != 4)
	{
		std::cout << "Insert: image_name - frames_number"<< std::endl;
		return 0;
	}

	// Declare class
	GTGenerator GTGen;
	GTGen.img_name = argv[1];
	//	int p = GTGen.img_name.size() - 11;
	std::string file_save;
	file_save = GTGen.img_name.substr(0, GTGen.img_name.size() - 11);
	std::cout << "save file " << file_save << std::endl;
	cv::namedWindow("Ground Truth Generation", CV_WINDOW_AUTOSIZE);
	char key;
	//	char* button1 = "button1";

	// set total number of frames
	GTGen.set_n_frames(atoi(argv[2]));
	GTGen.step_= atoi(argv[3]);
	int initialStep = GTGen.step_;


	cv::setMouseCallback("Ground Truth Generation", GTGen.onmouse, &GTGen);

	//	cv::createButton("Partial Occlusion", callbackButton_PO, &GTGen, CV_PUSH_BUTTON, false);
	//	cv::createButton("Total Occlusion", callbackButton_TO, &GTGen, CV_PUSH_BUTTON, false);
	//	cv::createButton("Out of Field", callbackButton_OFF, &GTGen, CV_PUSH_BUTTON, false);
	//	cv::createButton("Deformation", callbackButton_DEF, &GTGen, CV_PUSH_BUTTON, false);
	std::cout << __LINE__ << std::endl;
	GTGen.load_GTcontours(
			file_save + std::string("GT.xml"));
	std::cout << __LINE__ << std::endl;
	GTGen.load_Attributes(
			file_save + std::string("Attributes.xml"));
	std::cout << __LINE__ << std::endl;
	GTGen.refresh_ROI();
	std::cout << __LINE__ << std::endl;

	while(1)
	{
		key = (char)cv::waitKey(1);

		if (key == '0')
		{
			std::cout << "Set initial step [" << initialStep << "]" << std::endl;
			GTGen.step_ = initialStep;
		}
		if (key == '1')
		{
			std::cout << "Set step 1" << std::endl;
			GTGen.step_ = 1;
		}
		else if (key == '2')
		{
			std::cout << "Set step 5" << std::endl;
			GTGen.step_ = 5;
		}
		else if (key == '3')
		{
			std::cout << "Set step 10" << std::endl;
			GTGen.step_ = 10;
		}
		else if (key == '4')
		{
			std::cout << "Set step 50" << std::endl;
			GTGen.step_ = 50;
		}
		else if (key == '5')
		{
			std::cout << "Set step 100" << std::endl;
			GTGen.step_ = 100;
		}
		else if(key == 'n')
		{
			if(!GTGen.next_frame(GTGen.step_))
				continue;

		}
		else if(key == 'b')
		{
			if(!GTGen.prev_frame(GTGen.step_))
				continue;

		}
		else if(key == 'c')
		{
			GTGen.cancel_ROI();
		}
		else if(key == 'y')
		{
			callbackButton_PO(1, &GTGen);
		}
		else if(key == 'u')
		{
			callbackButton_TO(1, &GTGen);
		}
		else if(key == 'i')
		{
			callbackButton_OFF(1, &GTGen);
		}
		else if(key == 'o')
		{
			callbackButton_DEF(1, &GTGen);
		}
		else if(key == 'h')
		{
			callbackButton_IL(1, &GTGen);
		}
		else if(key == 'j')
		{
			callbackButton_CAM(1, &GTGen);
		}
		else if(key == 'k')
		{
			callbackButton_BLUR(1, &GTGen);
		}
		else if(key == 'l')
		{
			callbackButton_SMOKE(1, &GTGen);
		}
		else if(key == 'e')
		{
			GTGen.cancel_Attributes();
		}
		else if(key == 'q')
		{
			std::cout << "Exiting!" << std::endl;
			break;
		}
		else if(key == 's')
		{
			std::cout << "Save GT!" << std::endl;
			cv::FileStorage fs_GT;
			cv::FileStorage fs_Att;
			char varName[32];
			std::string  file_string_GT = file_save + std::string("GT.xml");
			std::string  file_string_Att = file_save + std::string("Attributes.xml");
			std::cout << "Saving data in " << file_string_GT << std::endl;
			fs_GT.open(file_string_GT, cv::FileStorage::WRITE);
			fs_Att.open(file_string_Att, cv::FileStorage::WRITE);

			// write GT
			if (!fs_GT.isOpened())
			{
				std::cout << "Failed to open "  << std::endl;
				return false;
			}
			else{
				for(int i = 0; i < GTGen.GT.size(); i++)
				{
					sprintf(varName, "GT%06d", i);
					fs_GT << varName << GTGen.getROI(i);
				}
			}

			fs_GT.release();

			// write Attribute
			if (!fs_Att.isOpened())
			{
				std::cout << "Failed to open "  << std::endl;
				return false;
			}
			else{
//				for(int i = 0; i < GTGen.GT.size(); i++)
//				{
//					sprintf(varName, "Att%06d", i);
				fs_Att << "Attributes" << GTGen.getAttribute();
//				}
			}

			fs_Att.release();


			continue;
		}
	}
}
