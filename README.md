Classifier
==========

Classifier desctiption


File structure
==============

The folders of the project are the following:

 - app     -> Folder containing source code of applications
 - etc     -> Folder containing non source code files (documents, configuration
   files, icons...)
   - cmake   -> CMake files (find files and other auxiliary files)
   - scripts -> Folder containing scripts
 - lib     -> Folder containing source code of libraries
 
In the root folder of the project it should be a CMakeLists.txt file to
compile the project with CMake.


Compilation
===========

These are the steps to compile Slither:

    $ cd path/to/classifier/source
    classifier$ mkdir build
    classifier$ cd build
    classifier/build$ ../etc/scripts/configure.sh    (see instructions below)
    classifier/build$ make
    classifier/build$ make install                   (the default folder is a local folder)

The configure script accepts several parameters:

 - (-h), (--help). Displays a help message
 - (-g), (--debug). Compiles in debug mode
 - (-r), (--release). Compiles in release mode
 - (--reset). Removes all the contents of the folder before calling CMake
 - (-DCMAKEVARIABLE). Calls CMake with some variable

A typical call to the configuration script would be:

    classifier/build$ ../etc/scripts/configure.sh --reset -r      (without compiling the modules)

Or:

    classifier/build$ ../etc/scripts/configure.sh --reset -r -m   (compiling the modules)

