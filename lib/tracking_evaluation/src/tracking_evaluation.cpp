#include<tracking_evaluation.h>

TrackingEvaluation::TrackingEvaluation()
{

}

TrackingEvaluation::~TrackingEvaluation()
{

}

/*
 * Load the GT contour
 */

void TrackingEvaluation::load_GTcontours(
		std::string file_name,
		int n_frames,
		cv::Mat* contours)
{
	cv::FileStorage fsGT;
	char varName[32];
	fsGT.open(file_name, cv::FileStorage::READ);

	if (!fsGT.isOpened())
	{
		std::cout << "Failed to open "  << std::endl;
		return;
	}
	else{
		for(int i = 0; i < n_frames; i++)
		{
			sprintf(varName,"GT%06d", i);
//			std::cout << varName << std::endl;
			fsGT[varName] >> contours[i];
		}
	}
	std::cout << "GT loaded!" << std::endl;
	fsGT.release();

}

/*
 * Load the TK safety area
 */

void TrackingEvaluation::load_TKcontours(
		std::string file_name,
		int n_frames,
		cv::Mat* contours)
{
	cv::FileStorage fsTK;
	char varName[32];
	fsTK.open(file_name, cv::FileStorage::READ);

	if (!fsTK.isOpened())
	{
		std::cout << "Failed to open "  << std::endl;
		return;
	}
	else{

		for(int i = 0; i < n_frames; i++)
		{
			sprintf(varName,"TK%06d", i);
			fsTK[varName] >> contours[i];

		}
	}
	std::cout << "TK loaded!" << std::endl;
	fsTK.release();

}

void TrackingEvaluation::load_TK_PO_contours(
		std::string file_name,
		int n_frames,
		cv::Mat* contours)
{
	cv::FileStorage fsTK_PO;
	char varName[64];
	std::cout << __LINE__ << std::endl;
	fsTK_PO.open(file_name, cv::FileStorage::READ);

	if (!fsTK_PO.isOpened())
	{
		std::cout << "Failed to open "  << std::endl;
		return;
	}
	else{
		for(int i = 0; i < n_frames; i++)
		{
			sprintf(varName,"TK_PO%06d", i);
			fsTK_PO[varName] >> contours[i];
//			std::cout << "var name" << (std::string)varName << std::endl;

		}
	}
	std::cout << "TK_PO loaded!" << std::endl;
	fsTK_PO.release();

}


void TrackingEvaluation::load_Attributes(
		std::string file_name,
		int n_frames,
		cv::Mat& contours)
{
	cv::FileStorage fsAtt;
	char varName[32];
	fsAtt.open(file_name, cv::FileStorage::READ);

	if (!fsAtt.isOpened())
	{
		std::cout << "Failed to open "  << std::endl;
		return;
	}
	else{

		fsAtt["Attributes"] >> contours;
	}

	std::cout << "Attributes loaded!" << std::endl;

	fsAtt.release();
}

void TrackingEvaluation::load_NContours(
		std::string file_name,
		int n_frames,
		cv::Mat& Ncontours)
{
	cv::FileStorage fsNC;
	fsNC.open(file_name, cv::FileStorage::READ);

	if (!fsNC.isOpened())
	{
		std::cout << "Failed to open "  << std::endl;
		return;
	}
	else{

		fsNC["NContour"] >> Ncontours;
	}

	std::cout << "N Contours loaded! " << std::endl;

	fsNC.release();
}

void TrackingEvaluation::save_overlap_ratio(
		std::string file_name,
		cv::Mat o_ratio)
{
	cv::FileStorage fs;
	fs.open(file_name, cv::FileStorage::WRITE);

	if (!fs.isOpened())
	{
		std::cout << "Failed to open "  << std::endl;
		return;
	}
	else{

		fs << "OverlapRatio" << o_ratio;
	}

	fs.release();
}

void TrackingEvaluation::set_image_size(
		std::string path_folder,
		cv::Size& img_size)
{
	cv::Mat img_temp;
	std::string temp_string = path_folder + "imL000000.png";
	img_temp = cv::imread(temp_string);
	img_size.width = img_temp.cols;
	img_size.height = img_temp.rows;
}
