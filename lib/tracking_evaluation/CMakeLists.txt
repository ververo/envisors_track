# Project configuration
project(tracking_evaluation_lib)

# Include directories
include_directories(include)


# Add library
add_library(tracking_evaluation_lib
  src/tracking_evaluation.cpp)
  

