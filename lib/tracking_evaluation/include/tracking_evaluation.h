#ifndef _TRACKINGEVALUATION_H_
#define _TRACKINGEVALUATION_H_


#include <stdio.h>
#include <stdlib.h>
#include <sstream>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

class TrackingEvaluation{
public:

	TrackingEvaluation();
	~TrackingEvaluation();

	//variables
	//methods

	void load_GTcontours(
			std::string file_name,
			int n_frames,
			cv::Mat* GT_contours);
	void load_TKcontours(
			std::string file_name,
			int n_frames,
			cv::Mat* GT_contours);
	void load_TK_PO_contours(
			std::string file_name,
			int n_frames,
			cv::Mat* contours);
	void load_Attributes(
			std::string file_name,
			int n_frames,
			cv::Mat& contours);
	void load_NContours(
			std::string file_name,
			int n_frames,
			cv::Mat& Ncontours);
	void save_overlap_ratio(
			std::string file_name,
			cv::Mat o_ratio);
	void set_image_size(
			std::string path_folder,
			cv::Size& img_size);

protected:

private:

	//variables
	//methods

};


#endif //_TRACKINGEVALUATION_H_
