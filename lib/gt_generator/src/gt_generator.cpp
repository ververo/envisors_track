#include<gt_generator.h>

GTGenerator::GTGenerator()
{
	this->pt_prev.x = 0;
	this->pt_prev.y = 0;
	this->pt_curr.x = 0;
	this->pt_curr.y = 0;
	this->n_frame = 0;

	this->end_ROI = false;

}

GTGenerator::~GTGenerator()
{

}

void GTGenerator::read_images(
		const std::string& img_name)
{

	char img_str[1024];

	//sprintf(img_str, "%s%04d.png", img_name.c_str(), i);
	sprintf(img_str, img_name.c_str(), this->n_frame);

	std::cout << "read image: " << img_str << std::endl;

	// read previous image
	this->img = cv::imread(img_str);

}



void GTGenerator::onmouse(
		int event,
		int x, int y,
		int flags,
		void* param)
{
	GTGenerator *gt = (GTGenerator*)param;

	//TODO
	//	enVisorsSys *enV = ((enVisorsSys*)(param));
	//	cv::Mat &img = enV->imgL_features; // 1st cast it back, then deref
	if(!gt->end_ROI){

		switch(event){

		case  CV_EVENT_LBUTTONUP:


			gt->pt_curr.x = x;
			gt->pt_curr.y = y;

			if(gt->pt_prev.x == 0 && gt->pt_prev.y == 0 )
			{
				gt->GT[gt->n_frame].ROI.clear();
			}

			// save selected points
			gt->GT[gt->n_frame].ROI.push_back(gt->pt_curr);


			if(gt->pt_prev.x != 0 && gt->pt_prev.y != 0 ){
				cv::line(gt->img, gt->pt_prev, gt->pt_curr,
						cv::Scalar(255, 0, 0), 2 );
			}

			cv::imshow("Ground Truth Generation", gt->img);


			gt->pt_prev = gt->pt_curr;

			break;

			//case  CV_EVENT_RBUTTONUP:
		case  CV_EVENT_MBUTTONUP:

			gt->end_ROI = true;
			gt->pt_curr.x = x;
			gt->pt_curr.y = y;


			if(!gt->GT[gt->n_frame].ROI.empty())
			{
				cv::line(gt->img, gt->pt_prev, gt->GT[gt->n_frame].ROI[0],
						cv::Scalar(255, 0, 0), 2 );
			}

			cv::imshow("Ground Truth Generation", gt->img);
			cv::waitKey(100);
			gt->next_frame(gt->step_);

			break;
		default:
			break;

		}
	}
}

void GTGenerator::set_n_frames(int n)
{
	if(n < 0)
	{
		std::cout << "Frame number < 0" << std::endl;
		return;
	}
	this->n_frames = n;

	GT.resize(this->n_frames);
}

bool GTGenerator::next_frame(int step)
{
	this->n_frame = this->n_frame + step;
	std::cout << "Next frame: " << this->n_frame << std::endl;
	if(this->n_frame >= this-> n_frames)
	{
		this->n_frame = this->n_frames - 1;
		std::cout << "Last frame already processed!" << std::endl;
		return false;
	}

	this->refresh_ROI();
	return true;
}

bool GTGenerator::prev_frame(int step)
{
	this->n_frame = this->n_frame - step;

	if(this->n_frame < 0)
	{
		this->n_frame = 0;
		std::cout << "No previous frame!" << std::endl;
		return false;
	}
	else
	{
		std::cout << "Previous frame: " << this->n_frame << std::endl;
	}

	this->refresh_ROI();
	return true;
}

void GTGenerator::refresh_ROI()
{
	this->pt_prev.x = 0;
	this->pt_prev.y = 0;
	this->end_ROI = false;

	this->read_images(
			this->img_name);

	if (!this->GT[this->n_frame].ROI.empty())
	{
		for(int i = 1; i < this->GT[this->n_frame].ROI.size(); i++)
		{
			cv::line(this->img, this->GT[this->n_frame].ROI[i-1],
					this->GT[this->n_frame].ROI[i],
					cv::Scalar(255, 0, 0), 2 );
		}

		cv::line(this->img, this->GT[this->n_frame].ROI[this->GT[this->n_frame].ROI.size()-1],
				this->GT[this->n_frame].ROI[0],
				cv::Scalar(255, 0, 0), 2 );
	}

	if (this->GT[this->n_frame].attribute == 1)
	{
		cv::putText(this->img, "Partial Occlusion", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
				4.0, cv::Scalar(255,0,0), 2);
	}
	else if(this->GT[this->n_frame].attribute == 2)
	{
		cv::putText(this->img, "Out of field", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
				4.0, cv::Scalar(255,0,0), 2);
	}
	else if(this->GT[this->n_frame].attribute == 3)
	{
		cv::putText(this->img, "Total Occlusion", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
				4.0, cv::Scalar(255,0,0), 2);
	}
	else if(this->GT[this->n_frame].attribute == 4)
	{
		cv::putText(this->img, "Deformation", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
				4.0, cv::Scalar(255,0,0), 2);
	}
	else if(this->GT[this->n_frame].attribute == 5)
	{
		cv::putText(this->img, "Illumination changes", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
				4.0, cv::Scalar(255,0,0), 2);
	}
	else if(this->GT[this->n_frame].attribute == 6)
	{
		cv::putText(this->img, "Abrupt camera motion", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
				4.0, cv::Scalar(255,0,0), 2);
	}
	else if(this->GT[this->n_frame].attribute == 7)
	{
		cv::putText(this->img, "Blur", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
				4.0, cv::Scalar(255,0,0), 2);
	}
	else if(this->GT[this->n_frame].attribute == 8)
	{
		cv::putText(this->img, "Smoke", cv::Point(60,60), cv::FONT_HERSHEY_PLAIN,
				4.0, cv::Scalar(255,0,0), 2);
	}

	cv::imshow("Ground Truth Generation", this->img);
}

void GTGenerator::cancel_ROI()
{
	this->GT[this->n_frame].ROI.clear();
	this->refresh_ROI();

}

void GTGenerator::cancel_Attributes()
{
	this->GT[this->n_frame].attribute = 0;
	this->refresh_ROI();
}

cv::Mat GTGenerator::getROI(int n_frame)
{
	cv::Mat roi;
	if(n_frame < 0 || n_frame >= this->n_frames)
	{
		std::cout << "Wrong frame number" << std::endl;
		return roi;
	}
	else
	{
		roi.create(this->GT[n_frame].ROI.size(),2, CV_32FC1);
		for(int i = 0; i < roi.rows; i++)
		{
			roi.at<float>(i,0) = this->GT[n_frame].ROI[i].x;
			roi.at<float>(i,1) = this->GT[n_frame].ROI[i].y;

		}
		return roi;
	}
}

cv::Mat GTGenerator::getAttribute(/*int n_frame*/)
{
//	cv::Mat attribute;
//	if(n_frame < 0 || n_frame >= this->n_frames)
//	{
//		std::cout << "Wrong frame number" << std::endl;
//		return attribute;
//	}
//	else
//	{
//		attribute.create(1,1, CV_8UC1);
//		attribute.at<int>(0,0) = this->GT[n_frame].attribute;
//
//		//			std::cout << "att" << this->GT[n_frame].attribute << std::endl;
//
//		return attribute;
//	}

	cv::Mat attribute;
		if(n_frame < 0 || n_frame >= this->n_frames)
		{
			std::cout << "Wrong frame number" << std::endl;
			return attribute;
		}
		else
		{
			attribute.create(this->n_frames,1, CV_8UC1);
			for(int i = 0; i < attribute.rows; i++)
			{
				attribute.at<unsigned char>(i,0) = this->GT[i].attribute;
			}


			//			std::cout << "att" << this->GT[n_frame].attribute << std::endl;

			return attribute;
		}
}

void GTGenerator::clear_GT_data(GroundTruth gt)
{
	gt.ROI.clear();
}


void GTGenerator::load_GTcontours(
		std::string file_name)
{
	cv::FileStorage fsGT;
	char varName[32];
	fsGT.open(file_name, cv::FileStorage::READ);
	cv::Mat tempROI;

	if (!fsGT.isOpened())
	{
		std::cout << "Failed to open "  << std::endl;
		return;
	}
	else{
		for(int i = 0; i < n_frames; i++)
		{
			sprintf(varName,"GT%06d", i);
			fsGT[varName] >> tempROI;
			for(int j = 0; j < tempROI.rows; j++)
			{
				cv::Point2f p;
				p.x = tempROI.at<float>(j, 0);
				p.y = tempROI.at<float>(j, 1);
				this->GT[i].ROI.push_back(p);
			}
		}
	}
	fsGT.release();
}

void GTGenerator::load_Attributes(
		std::string file_name)
{
	std::cout << __LINE__ << std::endl;
	cv::FileStorage fsAtt;
	char varName[32];
	fsAtt.open(file_name, cv::FileStorage::READ);
	cv::Mat tempROI;
	std::cout << __LINE__ << std::endl;
	if (!fsAtt.isOpened())
	{
		std::cout << "Failed to open "  << std::endl;
		return;
	}
	else{
		std::cout << __LINE__ << std::endl;
		cv::Mat att;
		fsAtt["Attributes"] >> att;
		std::cout << att.rows << std::endl;
		std::cout << "first element" << att.at<unsigned char>(0,0) << std::endl;

		for(int i = 0; i < n_frames; i++)
		{
			 this->GT[i].attribute = att.at<unsigned char>(i,0);
//			sprintf(varName,"Attributes", i);
//			fsAtt[varName] >> this->GT[i].attribute;
//			if(n_frames < 10)
//				std::cout << this->GT[i].attribute<< std::endl;
//			//			this->GT[i].attribute = tempROI.at<float>(i, 0);

		}
		std::cout << __LINE__ << std::endl;
	}
	fsAtt.release();
}
