#ifndef _GTGENERATOR_H_
#define _GTGENERATOR_H_


#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <string.h>
#include <unistd.h>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

struct GroundTruth
{
	std::vector<cv::Point> ROI;
	cv::Rect bounding_box;
	int attribute;
};


class GTGenerator{
public:

	GTGenerator();
	~GTGenerator();

	//variables
	std::vector<GroundTruth> GT;
	cv::Mat img;
	cv::Point pt_prev;
	cv::Point pt_curr;
	int n_frame;
	int n_frames;
	int step_;
	std::string img_name;
	bool end_ROI;

	//methods

	void read_images(
			const std::string& img_name);

	static void onmouse(
			int event,
			int x, int y,
			int flags,
			void* param);

	void set_n_frames(int n);

	bool next_frame(int step);

	bool prev_frame(int step);

	void refresh_ROI();

	void cancel_ROI();

	void cancel_Attributes();

	cv::Mat getROI(int n_frame);

	cv::Mat getAttribute(/*int n_frame*/);

	void clear_GT_data(GroundTruth gt);


	void load_GTcontours(
			std::string file_name);
	void load_Attributes(
			std::string file_name);

protected:

private:

	//variables
	//methods

};


#endif //_GTGENERATOR_H_
