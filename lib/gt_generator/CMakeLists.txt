# Project configuration
project(gt_generator_lib)

# Include directories
include_directories(include)


# Add library
add_library(gt_generator_lib
  src/gt_generator.cpp)
  

# Add test
add_executable(test_gt_generator
  test/test_gt_generator.cpp)
target_link_libraries(test_gt_generator
  gt_generator_lib
  ${OpenCV_LIBRARIES})
add_test(ClassifierTest
  ${EXECUTABLE_OUTPUT_PATH}/test_gt_generator)

