GT Generator
============

Usage
-----

        gt_generator IMAGE_FILE N_FRAMES STEP

Example:

        gt_generator imL%06d.png 100 10

Commands
--------

Steps control:

 - 0, Set initial step
 - 1, Set step 1
 - 2, Set step 5
 - 3, Set step 10
 - 4, Set step 50
 - 5, Set step 100

Frame control:

 - n, Next frame
 - b, Previous frame

ROI:

 - c, Cancel region of interest

Attributes:

 - y, Set attribute "Partial occlusion"
 - u, Set attribute "Total occlusion"
 - i, Set attribute "Out of field of view"
 - o, Set attribute "Deformation"
 - h, Set attribute "Illumination changes"
 - j, Set attribute "Abrupt camera motion"
 - k, Set attribute "Blur"
 - l, Set attribute "Smoke"
 - e, Cancel attributes

Data:

 - s, Save GT information

General:

 - q, Exit
